import { Component, OnInit } from '@angular/core';
import { Caregiver } from '../caregiver';
import { CaregiverService } from '../service/caregiver.service';
import{Router} from '@angular/router';

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent implements OnInit {
  caregivers:Caregiver[];
  constructor(private caregiverService:CaregiverService,private _router:Router) { }

  ngOnInit() {
    this.caregiverService.findAll().subscribe(data=>{
      this.caregivers=data;});
  }
updateCaregiver(caregiver){
  this.caregiverService.setter(caregiver);
  this._router.navigate(['caregivercreate']);
}
newCaregiver(){
  let caregiver=new Caregiver();
  this.caregiverService.setter(caregiver);
  this._router.navigate(['caregivercreate']);
}
deleteCaregiver(caregiver){
  this.caregiverService.deleteCaregiver(caregiver.id).subscribe((data)=>{
    this.caregivers.splice(this.caregivers.indexOf(caregiver),1);
  })
}
viewPatients(){
  this._router.navigate(['patient']);
}
viewMedication(){
  this._router.navigate(['medication']);
}
}
