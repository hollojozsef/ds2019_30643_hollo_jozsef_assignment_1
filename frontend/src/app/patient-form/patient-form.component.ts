import { Component, OnInit } from '@angular/core';
import {Patient} from '../patient';
import {Router} from '@angular/router';
import { PatientService } from '../service/patient.service';


@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent implements OnInit {
  private patient:Patient;
  constructor(private _patientService:PatientService,private _router:Router) { }

  ngOnInit() {
    this.patient=this._patientService.getter();
  }
  processForm(){
    if(this.patient.id=undefined){
      this._patientService.addPatient(this.patient).subscribe((patient)=>{
        console.log(patient);
        this._router.navigate(['/patient']);
      });
    }else{
      this._patientService.updatePatient(this.patient).subscribe((patient)=>{
        console.log(patient);
        this._router.navigate(['/patient']);
      });
    }
  }


}
