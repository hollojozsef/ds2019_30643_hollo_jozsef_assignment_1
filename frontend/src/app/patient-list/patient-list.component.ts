import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { PatientService } from '../service/patient.service';
import{Router} from '@angular/router';
@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
 patients:Patient[];
  constructor(private patientService:PatientService,private _router:Router) { }

  ngOnInit() {
    this.patientService.findAll().subscribe(data=>{
      this.patients=data;});
  }
updatePatient(patient){
  this.patientService.setter(patient);
  this._router.navigate(['patientcreate']);
}
newPatient(){
  let patient=new Patient();
  this.patientService.setter(patient);
  this._router.navigate(['patientcreate']);
}
deletePatient(patient){
  this.patientService.deletePatient(patient.id).subscribe((data)=>{
    this.patients.splice(this.patients.indexOf(patient),1);
  })
}
goBack(){
  this._router.navigate(['caregiver']);
  }
}
