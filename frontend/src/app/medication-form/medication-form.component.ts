import { Component, OnInit } from '@angular/core';
import { Medication } from '../medication';
import { MedicationService } from '../service/medication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medication-form',
  templateUrl: './medication-form.component.html',
  styleUrls: ['./medication-form.component.css']
})
export class MedicationFormComponent implements OnInit {
  private medication:Medication;
  constructor(private _patientService:MedicationService,private _router:Router) { }

  ngOnInit() {
    this.medication=this._patientService.getter();
  }
  processForm(){
    if(this.medication.id=undefined){
      this._patientService.addMedication(this.medication).subscribe((medication)=>{
        console.log(medication);
        this._router.navigate(['/medication']);
      });
    }else{
      this._patientService.updateMedication(this.medication).subscribe((medication)=>{
        console.log(medication);
        this._router.navigate(['/medication']);
      });
    }
  }

}
