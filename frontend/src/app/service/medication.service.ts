import { Injectable } from '@angular/core';
import { Medication } from '../medication';
import { Observable,of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://localhost:8080/medication";
@Injectable({
  providedIn: 'root'
})
export class MedicationService {
  private medication:Medication;

  constructor(private http: HttpClient) {
   }
   public findAll():Observable<Medication[]>{
    const url=`${apiUrl}/all`;
     return this.http.get<Medication[]>(url);
    }
    setter(medication:Medication){
      this.medication=medication;
    }
    getter(){
      return this.medication;
    }
    addMedication (medication): Observable<Medication> {
      const url=`${apiUrl}/add`;
      return this.http.post<Medication>(url, medication, httpOptions).pipe(
        tap((patient: Medication) => console.log(`added product w/ id=${medication.id}`))
      );
    }
    updateMedication(medication): Observable<Medication> {
      const url=`${apiUrl}/update`;
      return this.http.put<Medication>(url, medication, httpOptions).pipe(
        tap((medication: Medication) => console.log(`updated product w/ id=${medication.id}`))
      );
    }
    deleteMedication(id:number):Observable<Medication>{
      const url = `${apiUrl}/delete/${id}`;
      return this.http.delete<Medication>(url, httpOptions).pipe(
        tap(_ => console.log(`deleted product id=${id}`))
      );
    }
}
