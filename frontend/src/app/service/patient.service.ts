import { Injectable } from '@angular/core';
import { Patient } from '../patient';
import { Observable,of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://localhost:8080/patient";
@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private patient:Patient;

  constructor(private http: HttpClient) {
   }
   public findAll():Observable<Patient[]>{
    const url=`${apiUrl}/all`;
     return this.http.get<Patient[]>(url);
    }
    setter(patient:Patient){
      this.patient=patient;
    }
    getter(){
      return this.patient;
    }
    addPatient (patient): Observable<Patient> {
      const url=`${apiUrl}/add`;
      return this.http.post<Patient>(url, patient, httpOptions).pipe(
        tap((patient: Patient) => console.log(`added product w/ id=${patient.id}`))
      );
    }
    updatePatient(patient): Observable<Patient> {
      const url=`${apiUrl}/update`;
      return this.http.put<Patient>(url, patient, httpOptions).pipe(
        tap((patient: Patient) => console.log(`updated product w/ id=${patient.id}`))
      );
    }
    deletePatient(id:number):Observable<Patient>{
      const url = `${apiUrl}/delete/${id}`;
      return this.http.delete<Patient>(url, httpOptions).pipe(
        tap(_ => console.log(`deleted product id=${id}`))
      );
    }
    getPatient(id: number): Observable<Patient> {
      const url = `${apiUrl}/${id}`;
      return this.http.get<Patient>(url).pipe(
        tap(_ => console.log(`fetched Patient id=${id}`))
        );
    }
    
}
