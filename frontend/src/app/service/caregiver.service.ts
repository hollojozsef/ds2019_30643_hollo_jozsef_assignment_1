import { Injectable } from '@angular/core';
import { Caregiver } from '../caregiver';
import { Observable,of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://localhost:8080/caregiver";
@Injectable({
  providedIn: 'root'
})
export class CaregiverService {
  private caregiver:Caregiver;

  constructor(private http: HttpClient) {
   }
   public findAll():Observable<Caregiver[]>{
    const url=`${apiUrl}/all`;
     return this.http.get<Caregiver[]>(url);
    }
    setter(caregiver:Caregiver){
      this.caregiver=caregiver;
    }
    getter(){
      return this.caregiver;
    }
    addCaregiver (caregiver): Observable<Caregiver> {
      const url=`${apiUrl}/add`;
      return this.http.post<Caregiver>(url, caregiver, httpOptions).pipe(
        tap((caregiver: Caregiver) => console.log(`added product w/ id=${caregiver.id}`))
      );
    }
    updateCaregiver(caregiver): Observable<Caregiver> {
      const url=`${apiUrl}/update`;
      return this.http.put<Caregiver>(url, caregiver, httpOptions).pipe(
        tap((caregiver: Caregiver) => console.log(`updated product w/ id=${caregiver.id}`))
      );
    }
    deleteCaregiver(id:number):Observable<Caregiver>{
      const url = `${apiUrl}/delete/${id}`;
      return this.http.delete<Caregiver>(url, httpOptions).pipe(
        tap(_ => console.log(`deleted product id=${id}`))
      );
    }
}
