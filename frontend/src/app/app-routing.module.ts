import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DoctorComponent} from './doctor/doctor.component';
import {DoctorListComponent} from './doctor-list/doctor-list.component';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverFormComponent } from './caregiver-form/caregiver-form.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { MedicationListComponent } from './medication-list/medication-list.component';
import { MedicationFormComponent } from './medication-form/medication-form.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [{path:'doctor',component:DoctorComponent},
{path:'all',component:DoctorListComponent},
{path:'caregiver',component:CaregiverListComponent},
{path:'caregivercreate',component:CaregiverFormComponent},
{path:'patient',component:PatientListComponent},
{path:'patientcreate',component:PatientFormComponent},
{path:'medication',component:MedicationListComponent},
{path:'medicationcreate',component:MedicationFormComponent},
{path: 'login', component: LoginComponent},
  {path: '', component: LoginComponent},
  {path: 'logout', component: LoginComponent},
  {path: 'hello-world', component: DoctorComponent},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents=[DoctorComponent];
