import { Component, OnInit } from '@angular/core';
import { CaregiverService } from '../service/caregiver.service';
import {Caregiver} from '../caregiver';
import {Router} from '@angular/router';


@Component({
  selector: 'app-caregiver-form',
  templateUrl: './caregiver-form.component.html',
  styleUrls: ['./caregiver-form.component.css']
})
export class CaregiverFormComponent implements OnInit {
  private caregiver:Caregiver;
  constructor(private _caregiverService:CaregiverService,private _router:Router) { }

  ngOnInit() {
    this.caregiver=this._caregiverService.getter();
  }
  processForm(){
    if(this.caregiver.id=undefined){
      this._caregiverService.addCaregiver(this.caregiver).subscribe((caregiver)=>{
        console.log(caregiver);
        this._router.navigate(['/caregiver']);
      });
    }else{
      this._caregiverService.updateCaregiver(this.caregiver).subscribe((caregiver)=>{
        console.log(caregiver);
        this._router.navigate(['/caregiver']);
      });
    }
  }

}
