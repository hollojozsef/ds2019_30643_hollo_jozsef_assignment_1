package com.lab1.lab1.repository;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient,Long> {
    Patient findById(long id);
    void deleteById(long id);
}
