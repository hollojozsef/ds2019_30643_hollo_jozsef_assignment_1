package com.lab1.lab1.service;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.entities.Medication;
import com.lab1.lab1.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicationService {
    @Autowired
    MedicationRepository repository;
    public List<Medication> getAllMedication(){
        List<Medication> caregiverList=repository.findAll();
        if(caregiverList.size()>0)
            return caregiverList;
        else
            return new ArrayList<Medication>();
    }
    public Medication getMedicationById(long id){
        return repository.findById(id);
    }
    public void addMedication(Medication medication){
        repository.save(medication);
    }
    public void deleteMedication(long id){
        repository.deleteById(id);
    }
    public Medication setMedication(Medication medication){
        return repository.save(medication);
    }
}
