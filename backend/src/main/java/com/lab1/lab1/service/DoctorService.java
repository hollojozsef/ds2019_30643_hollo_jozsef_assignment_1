package com.lab1.lab1.service;

import com.lab1.lab1.entities.Doctor;
import com.lab1.lab1.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service

public class DoctorService {
    @Autowired
    DoctorRepository repository;

    public List<Doctor> getAllDoctors(){
        List<Doctor> doctorList=repository.findAll();
        if(doctorList.size()>0)
            return doctorList;
        else
            return new ArrayList<Doctor>();
    }
    public Doctor getDoctorById(long id){
        return repository.findById(id);
    }
    public void addDoctor(Doctor doctor){
        repository.save(doctor);
    }
    public void deleteDoctor(long id){
        repository.deleteById(id);
    }
    public Doctor setDoctor(Doctor doctor){
        return repository.save(doctor);
    }
}
